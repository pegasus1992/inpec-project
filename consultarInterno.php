<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'imagen.php';
include_once 'controller/consultarInternosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Interno</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $interno = getInterno($_GET['nui'], $_GET['td']);
            ?>
            <form action="modificarInterno.php" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Consultar Interno</h4>
                <div>
                    <label for="nui">Numero NUI:</label>
                    <?php echo '<input name="nui" id="nui" type="number" value="'.$interno->getNui().'" readonly/>' ?>
                </div>
                <div>
                    <label for="td">Numero TD:</label>
                    <?php echo '<input name="td" id="td" type="text" value="'.$interno->getTd().'" readonly/>' ?>
                </div>
                <!--div>
                    <label>Establecimiento:</label>
                    <--?php echo '<input name="establecimiento" id="establecimiento" type="text" value="'.$interno->getEstablecimiento()->getNombre().'" readonly/>' ?>
                </div-->
                <div>
                    <label for="nombres">Nombres:</label>
                    <?php echo '<input name="nombres" id="nombres" type="text" value="'.$interno->getNombres().'" readonly/>' ?>
                </div>
                <div>
                    <label for="apellidos">Apellidos:</label>
                    <?php echo '<input name="apellidos" id="apellidos" type="text" value="'.$interno->getApellidos().'" readonly/>' ?>
                </div>
                <div>
                    <label for="nacionalidad">Nacionalidad:</label>
                    <?php echo '<input name="nacionalidad" id="nacionalidad" type="text" value="'.$interno->getNacionalidad().'" readonly/>' ?>
                </div>
                <div>
                    <label for="situacion">Situación Jurídica:</label>
                    <?php
                    if($interno->getSituacionJuridica() == "S") {
                        echo '<input name="situacion" id="situacion" type="text" value="Sindicado" readonly/>';
                    } else if($interno->getSituacionJuridica() == "C") {
                        echo '<input name="situacion" id="situacion" type="text" value="Condenado" readonly/>';
                    } else {
                        echo '<input name="situacion" id="situacion" type="text" value="" readonly/>';
                    }
                    ?>
                </div>
                <div>
                    <label for="delito">Delito:</label>
                    <?php echo '<textarea cols="40" rows="5" name="delito" id="delito" type="text" readonly>'.$interno->getDelito().'</textarea>' ?>
                </div>
                <div>
                    <label for="observaciones">Observaciones:</label>
                    <?php echo '<textarea cols="40" rows="5" name="observaciones" id="observaciones" type="text" readonly>'.$interno->getObservaciones().'</textarea>' ?>
                </div>
                <div>
                    <label for="fechaIngreso">Fecha de Ingreso:</label>
                    <?php echo '<input name="fechaIngreso" id="fechaIngreso" type="date" value="'.$interno->getFechaIngreso().'" readonly/>' ?>
                </div>
                <div>
                    <label for="fechaSalida">Fecha de Salida:</label>
                    <?php echo '<input name="fechaSalida" id="fechaSalida" type="date" value="'.$interno->getFechaSalida().'" readonly/>' ?>
                </div>
                <div>
                    <label>Foto:</label>
                    <?php
                    $foto = $interno->getFoto();
                    if($foto->getId() != null) {
                        echo imageField("imagen", $foto->getId());
                    } else {
                        echo imageField("imagen");
                    }
                    ?>
                </div>
                <div align="center">
                    <input name="modify" id="submit" type="submit" value="Modificar Interno" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='consultarInternos.php'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}