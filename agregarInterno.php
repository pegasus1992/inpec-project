<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarInternoController.php';
include_once 'controller/consultarEstablecimientosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar Interno</title>';
        ?>
        <!--link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
        <script src="view/js/datePicker.js" type="text/javascript"></script-->
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Agregar Interno</h4>
                <div>
                    <label for="nui">Numero NUI (*):</label>
                    <input placeholder="Ingrese el NUI" name="nui" id="nui" type="number" autocomplete="off" min="1" max="999999999" required/>
                </div>
                <div>
                    <label for="td">Numero TD (*):</label>
                    <input placeholder="Ingrese el TD" name="td" id="td" type="text" autocomplete="off" maxlength="6" required/>
                </div>
                <!--p>
                    <label>Establecimiento:</label>
                    <select name="selectEstablecimiento" id="selectEstablecimiento" required>
                        <option></option>
                        <!--?php
                        $establecimientos = getEstablecimientos();
                        foreach($establecimientos as $establecimiento) {
                            $td = $establecimiento->getTd();
                            $nombre = $establecimiento->getNombre();
                            if($td == 113) {
                                echo '<option value="'.$td.'">'.$nombre.'</option>';
                            }
                        }
                        ?>
                    </select>
                </p-->
                <div>
                    <label for="nombres">Nombres (*):</label>
                    <input placeholder="Ingrese los nombres" name="nombres" id="nombres" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="apellidos">Apellidos (*):</label>
                    <input placeholder="Ingrese los apellidos" name="apellidos" id="apellidos" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="nacionalidad">Nacionalidad:</label>
                    <input placeholder="Ingrese la nacionalidad" name="nacionalidad" id="nacionalidad" type="text" autocomplete="off"/>
                </div>
                <div>
                    <label for="situacion">Situación Jurídica:</label>
                    <select name="situacion" id="situacion">
                        <option></option>
                        <option value="S">Sindicado</option>
                        <option value="C">Condenado</option>
                    </select>
                </div>
                <div>
                    <label for="delito">Delito (*):</label>
                    <textarea cols="40" rows="5" placeholder="Ingrese el delito" name="delito" id="delito" type="text" autocomplete="off" required></textarea>
                </div>
                <div>
                    <label for="observaciones">Observaciones:</label>
                    <textarea cols="40" rows="5" placeholder="Ingrese las observaciones pertinentes" name="observaciones" id="observaciones" type="text" autocomplete="off"></textarea>
                </div>
                <div>
                    <label for="fechaIngreso">Fecha de Ingreso:</label>
                    <input type="date" id="fechaIngreso" name="fechaIngreso" class="fecha" placeholder="yyyy-mm-dd"/>
                </div>
                <div>
                    <label for="fechaSalida">Fecha de Salida:</label>
                    <input type="date" id="fechaSalida" name="fechaSalida" class="fecha" placeholder="yyyy-mm-dd"/>
                </div>
                <div>
                    <label for="foto">Foto:</label>
                    <input placeholder="Ingrese la foto" name="foto" id="foto" type="file" accept="image/*" autocomplete="off"/>
                    <br/>
                    <br/>
                    <?php
                    echo imageField("imagen");
                    ?>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
        <!-- CARGAR IMAGEN -->
        <script src="view/js/fotoInternoCharger.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}