<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'utils/funciones.php';
include_once 'controller/loginController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Home Page</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div align="center" style="width:800px" class="texto2">
            <h1 class="Titulo">Página de Inicio</h1>
        </div>
        <div style="width:300px" class="texto2">
            <?php
            echo '<p class="LinkLogin">Bienvenido, '.$_SESSION['nombre'].'</p>';
            ?>
            <a href="logout.php" class="LinkLogin3">Cerrar Sesión</a>
            <br/>
            <br/>
        </div>
        <br/>
        <br/>
        <!--p>
            <h2>Catálogo de Establecimientos</h2>
            <input type="button" value="Agregar uno" onclick="window.location='agregarEstablecimiento.php'">
            <input type="button" value="Consultar Todos" onclick="window.location='consultarEstablecimientos.php'">
        </p>
        <br/-->
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Internos</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarInterno.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarInternos.php'">
            <br/>
            <br/>
        </div>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Grupos de Productos</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarGrupo.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarGrupos.php'">
            <br/>
            <br/>
        </div>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Productos</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarProducto.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarProductos.php'">
            <br/>
            <br/>
        </div>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Bodegas</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarBodega.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarBodegas.php'">
            <br/>
            <br/>
        </div>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Documentos Fuente</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarDocumentoFuente.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarDocumentosFuente.php'">
            <br/>
            <br/>
        </div>
        <div style="width:350px" class="texto2">
            <h4>Catálogo de Proveedores</h4>
            <input type="button" class="Button2" value="Agregar uno" onclick="window.location='agregarProveedor.php'">
            <input type="button" class="Button2" value="Consultar todos" onclick="window.location='consultarProveedores.php'">
            <br/>
            <br/>
        </div>
        <!--p>
            <h2>Registro de Transacciones</h2>
            <p>
                <input type="button" value="Entradas de Proveedor" onclick="window.location='registrarEntradaProveedores.php'">
                <input type="button" value="Salidas a Proveedor" onclick="window.location=''">
            </p>
            <p>
                <input type="button" value="Entradas de Cliente" onclick="window.location=''">
                <input type="button" value="Salidas a Cliente" onclick="window.location=''">
            </p>
        </p>
        <p>
            <h2>Kárdex e Inventarios</h2>
            <input type="button" value="Ver Kárdex" onclick="window.location=''">
            <input type="button" value="Ver Inventarios" onclick="window.location=''">
        </p-->
    </body>
</html>
    <?php
} else {
    redirigir("./");
}