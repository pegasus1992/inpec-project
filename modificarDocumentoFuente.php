<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarDocumentosFuenteController.php';
include_once 'controller/modificarDocumentoFuenteController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Modificar Documento Fuente</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $docFuente = getDocumentoFuente($_POST['sigla']);
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Modificar Documento Fuente</h4>
                <div>
                    <label for="sigla">Sigla:</label>
                    <?php echo '<input name="sigla" id="sigla" type="text" value="'.$docFuente->getSigla().'" readonly/>' ?>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <?php echo '<input name="nombre" id="nombre" type="text" value="'.$docFuente->getNombre().'" readonly/>' ?>
                </div>
                <div>
                    <label>Actúa en:</label>
                    <radio>
                        <?php
                        if($docFuente->getAccion() == "E") {
                            echo '<input type="radio" name="accion" value="E" required checked/>Entradas';
                            echo '<input type="radio" name="accion" value="S" required/>Salidas';
                        } else {
                            echo '<input type="radio" name="accion" value="E" required/>Entradas';
                            echo '<input type="radio" name="accion" value="S" required checked/>Salidas';
                        }
                        ?>
                    </radio>
                </div>
                <div>
                    <label>Aplica a:</label>
                    <radio>
                        <?php
                        if($docFuente->getAplica() == "P") {
                            echo '<input type="radio" name="aplica" value="P" required checked/>Proveedores';
                            echo '<input type="radio" name="aplica" value="C" required/>Clientes';
                        } else {
                            echo '<input type="radio" name="aplica" value="P" required/>Proveedores';
                            echo '<input type="radio" name="aplica" value="C" required checked/>Clientes';
                        }
                        ?>
                    </radio>
                </div>
                <div>
                    <label>¿Tiene IVA? </label>
                    <radio>
                        <?php
                        if($docFuente->getIva() == "S") {
                            echo '<input type="radio" name="iva" value="S" required checked/>Sí';
                            echo '<input type="radio" name="iva" value="N" required/>No';
                        } else {
                            echo '<input type="radio" name="iva" value="S" required/>Sí';
                            echo '<input type="radio" name="iva" value="N" required checked/>No';
                        }
                        ?>
                    </radio>
                </div>
                <div>
                    <label>¿Tiene vencimiento? </label>
                    <radio>
                        <?php
                        if($docFuente->getVencimiento() == "S") {
                            echo '<input type="radio" name="vencimiento" value="S" required checked/>Sí';
                            echo '<input type="radio" name="vencimiento" value="N" required/>No';
                        } else {
                            echo '<input type="radio" name="vencimiento" value="S" required/>Sí';
                            echo '<input type="radio" name="vencimiento" value="N" required checked/>No';
                        }
                        ?>
                    </radio>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <?php
                    echo '<input name="return" type="button" value="Regresar" class="Button2" onclick="window.location=\'consultarDocumentoFuente.php?sigla='.$docFuente->getSigla().'\'">';
                    ?>
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}