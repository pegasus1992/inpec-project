<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarDocumentoFuenteController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar Documento Fuente</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Agregar Documento Fuente</h4>
                <div>
                    <label for="sigla">Sigla:</label>
                    <input placeholder="Ingrese la sigla" name="sigla" id="sigla" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <input placeholder="Ingrese el nombre" name="nombre" id="nombre" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label>Actúa en:</label>
                    <radio>
                        <input type="radio" name="accion" value="E" required/>Entradas
                        <input type="radio" name="accion" value="S" required/>Salidas
                    </radio>
                </div>
                <div>
                    <label>Aplica a:</label>
                    <radio>
                        <input type="radio" name="aplica" value="P" required/>Proveedores
                        <input type="radio" name="aplica" value="C" required/>Clientes
                    </radio>
                </div>
                <div>
                    <label>¿Tiene IVA? </label>
                    <radio>
                        <input type="radio" name="iva" value="S" required/>Sí
                        <input type="radio" name="iva" value="N" required/>No
                    </radio>
                </div>
                <div>
                    <label>¿Tiene vencimiento? </label>
                    <radio>
                        <input type="radio" name="vencimiento" value="S" required/>Sí
                        <input type="radio" name="vencimiento" value="N" required/>No
                    </radio>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}