<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'controller/consultarGruposController.php';
include_once 'model/Producto.php';

if(isset($_POST['register'])) {
    $codigoBarras = $_POST['codigo'];
    $nombre = $_POST['nombre'];
    $grupo = $_POST['selectGrupo'];
    $iva = $_POST['iva'];
    $puntoPedido = $_POST['puntoPedido'];
    $stockMinimo = $_POST['stockMinimo'];
    
    $tables = array("codigoBarras", "nombre", "puntoPedido", "stockMinimo");
    $values = array("'$codigoBarras'", "'$nombre'", "$puntoPedido", "$stockMinimo");
    
    if($grupo != null) {
        $tables[] = "grupo";
        $values[] = "'$grupo'";
    }
    if($iva != null) {
        $tables[] = "iva";
        $values[] = "$iva";
    }
    
    $insertInto = "";
    for($i=0; $i<sizeof($tables); $i++) {
        $insertInto .= $tables[$i];
        if($i < sizeof($tables)-1) {
            $insertInto .= ", ";
        }
    }
    $insertValues = "";
    for($i=0; $i<sizeof($values); $i++) {
        $insertValues .= $values[$i];
        if($i < sizeof($values)-1) {
            $insertValues .= ", ";
        }
    }
    
    $insert = "INSERT INTO Producto ($insertInto) VALUES ($insertValues)";
    if($puntoPedido >= $stockMinimo) {
        if(executeSimpleQuery($insert)) {
            mostrarMensaje("Producto registrado correctamente");
            redirigir("home.php");
        }
    }
    
}