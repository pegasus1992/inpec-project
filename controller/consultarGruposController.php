<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Grupo.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE Grupo SET estado = 'I' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarGrupos.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE Grupo SET estado = 'A' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarGrupos.php");
    }
}

/**
 * Obtiene todos los grupos registrados.
 * @return \Grupo
 */
function getGrupos($filtro = "") {
    $sql = "SELECT DISTINCT * FROM Grupo ";
    $result = getResultSet($sql.$filtro);
    $grupos = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $grupo = new Grupo($fila['sigla'], $fila['nombre'], $fila['estado']);
            $grupos[] = $grupo;
        }
    }
    return $grupos;
}