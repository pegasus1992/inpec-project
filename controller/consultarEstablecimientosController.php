<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Establecimiento.php';

/**
 * Obtiene todos los establecimientos registrados.
 * @return \Establecimiento
 */
function getEstablecimientos() {
    $sql = "SELECT * FROM Establecimiento";
    $result = getResultSet($sql);
    $establecimientos = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $establecimiento = new Establecimiento($fila['td'], $fila['nombre']);
            $establecimientos[] = $establecimiento;
        }
    }
    return $establecimientos;
}