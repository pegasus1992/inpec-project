<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';

if(isset($_POST['register'])) {
    $nui = $_POST['nui'];
    $td = $_POST['td'];
    $establecimiento = $_POST['selectEstablecimiento'];
    $nombres = $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $nacionalidad = $_POST['nacionalidad'];
    $situacion = $_POST['situacion'];
    $delito = $_POST['delito'];
    $observaciones = $_POST['observaciones'];
    $fechaIngreso = $_POST['fechaIngreso'];
    $fechaSalida = $_POST['fechaSalida'];
    $foto = $_FILES['foto'];
    
    $tables = array("nui", "td", "tdEstablecimiento", "nombres", "apellidos", "delito");
    $values = array("'$nui'", "'$td'", /*"$establecimiento"*/113, "'$nombres'", "'$apellidos'", "'$delito'");
    $datosImagen = null;
    $pdo = connectPDO();
    
    if($nacionalidad != null) {
        array_push($tables, "nacionalidad");
        array_push($values, "'$nacionalidad'");
    }
    if($situacion != null) {
        array_push($tables, "situacionJuridica");
        array_push($values, "'$situacion'");
    }
    if($observaciones != null) {
        array_push($tables, "observaciones");
        array_push($values, "'$observaciones'");
    }
    if($fechaIngreso != null) {
        array_push($tables, "fechaIngreso");
        array_push($values, "'$fechaIngreso'");
    }
    if($fechaSalida != null) {
        array_push($tables, "fechaSalida");
        array_push($values, "'$fechaSalida'");
    }
    if($foto['name'] != null) {
        array_push($tables, "foto");
        
        $imagePath = $foto['tmp_name'];
        $imageSize = $foto['size'];
        $imageName = $foto['name'];
        $imageType = $foto['type'];
        
        $fp = fopen($imagePath, 'r');
        if($fp) {
            $datosImagen = fread($fp, $imageSize);//Cargar la imagen
            fclose($fp);
        }
        if($datosImagen != null) {
            $datosImagen = base64_encode($datosImagen);
            $sql = "INSERT INTO Foto (imagen, tipo) VALUES ('$datosImagen', '$imageType')";
            if(executeQueryNoCommit($pdo, $sql)) {
                //Consultar el id autogenerado
                $sql = "SELECT id FROM Foto WHERE imagen = '$datosImagen' && tipo = '$imageType'";
                $rec = getResultSetNoCommit($pdo, $sql);
                $row = mysqli_fetch_object($rec);
                $result = $row;
                
                array_push($values, $result->id);
            }
        }
    }
    
    $insertInto = "";
    for($i=0; $i<sizeof($tables); $i++) {
        $insertInto .= $tables[$i];
        if($i < sizeof($tables)-1) {
            $insertInto .= ", ";
        }
    }
    $insertValues = "";
    for($i=0; $i<sizeof($values); $i++) {
        $insertValues .= $values[$i];
        if($i < sizeof($values)-1) {
            $insertValues .= ", ";
        }
    }
    
    $insert = "INSERT INTO Interno ($insertInto) VALUES ($insertValues)";
    if(executeQueryCommit($pdo, $insert)) {
        mostrarMensaje("Interno registrado correctamente");
        redirigir("home.php");
    }
}