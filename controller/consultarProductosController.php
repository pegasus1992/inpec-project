<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Producto.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE Producto SET estado = 'I' WHERE codigoBarras = '" . $_GET['codigo'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarProductos.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE Producto SET estado = 'A' WHERE codigoBarras = '" . $_GET['codigo'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarProductos.php");
    }
} else if(isset($_GET['details'])) {
    redirigir("consultarProducto.php?codigo=".$_GET['codigo']);
}

/**
 * Obtiene todos los productos registrados.
 * @return \Producto
 */
function getProductos() {
    $result = traerProductos();
    $productos = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $producto = almacenarProducto($fila);
            $productos[] = $producto;
        }
    }
    return $productos;
}

/**
 * Obtiene todos los productos activos.
 * @return type
 */
function getProductosActivos() {
    $filtro = "WHERE P.estado = 'A'";
    $result = traerProductos($filtro);
    $productos = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $producto = almacenarProducto($fila);
            $productos[] = $producto;
        }
    }
    return $productos;
}

/**
 * Obtiene la informacion del producto, dado su codigo de barras.
 * @param type $codigo
 * @return type
 */
function getProducto($codigo) {
    $filtro = "WHERE P.codigoBarras = '$codigo'";
    $result = traerProductos($filtro);
    $producto = null;
    if($result->num_rows > 0) {
        $fila = mysqli_fetch_array($result);
        $producto = almacenarProducto($fila);
    }
    return $producto;
}

/**
 * Almacena el producto traido de la BD.
 * @param type $fila Producto de la BD.
 * @return \Producto Objeto tipo Producto.
 */
function almacenarProducto($fila) {
    $grupo = new Grupo($fila['siglaGrupo'], $fila['nombreGrupo'], $fila['estadoGrupo']);
    $producto = new Producto($fila['codigoProducto'], $fila['nombreProducto'], $grupo, $fila['iva'], $fila['puntoPedido'], $fila['stockMinimo'], $fila['estadoProducto']);
    return $producto;
}

/**
 * Trae los productos de la BD.
 * @param type $filtro Filtro para buscar un producto específico. Por defecto busca todos los productos.
 * @return type
 */
function traerProductos($filtro = "") {
    $sql = "SELECT DISTINCT G.sigla siglaGrupo, G.nombre nombreGrupo, G.estado estadoGrupo, P.codigoBarras codigoProducto, "
            . "P.nombre nombreProducto, P.iva, P.puntoPedido, P.stockMinimo, P.estado estadoProducto "
            . "FROM Producto P LEFT JOIN Grupo G ON P.grupo = G.sigla ";
    return getResultSet($sql.$filtro);
}