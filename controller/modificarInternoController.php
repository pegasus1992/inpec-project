<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';

if(isset($_POST['register'])) {
    $nui = $_POST['nui'];
    $td = $_POST['td'];
    $establecimiento = $_POST['selectEstablecimiento'];
    $nombres = $_POST['nombres'];
    $apellidos = $_POST['apellidos'];
    $nacionalidad = $_POST['nacionalidad'];
    $situacion = $_POST['situacion'];
    $delito = $_POST['delito'];
    $observaciones = $_POST['observaciones'];
    $fechaIngreso = $_POST['fechaIngreso'];
    $fechaSalida = $_POST['fechaSalida'];
    $estado = $_POST['selectEstado'];
    $foto = $_FILES['foto'];
    
    $set = array("nombres = '$nombres'", "apellidos = '$apellidos'", "delito = '$delito'", "estado = '$estado'", "tdEstablecimiento = 113");
    $datosImagen = null;
    $pdo = connectPDO();
    
    if($nacionalidad != null) {
        $set[] = "nacionalidad = '$nacionalidad'";
    } else {
        $set[] = "nacionalidad = null";
    }
    if($situacion != null) {
        $set[] = "situacionJuridica = '$situacion'";
    } else {
        $set[] = "situacionJuridica = null";
    }
    if($observaciones != null) {
        $set[] = "observaciones = '$observaciones'";
    } else {
        $set[] = "observaciones = null";
    }
    if($fechaIngreso != null) {
        $set[] = "fechaIngreso = '$fechaIngreso'";
    } else {
        $set[] = "fechaIngreso = CURRENT_TIMESTAMP";
    }
    if($fechaSalida != null) {
        $set[] = "fechaSalida = '$fechaSalida'";
    } else {
        $set[] = "fechaSalida = null";
    }
    if($foto['name'] != null) {
        $imagePath = $foto['tmp_name'];
        $imageSize = $foto['size'];
        $imageName = $foto['name'];
        $imageType = $foto['type'];
        
        $fp = fopen($imagePath, 'r');
        if($fp) {
            $datosImagen = fread($fp, $imageSize);//Cargar la imagen
            fclose($fp);
        }
        if($datosImagen != null) {
            $datosImagen = base64_encode($datosImagen);
            $sql = "INSERT INTO Foto (imagen, tipo) VALUES ('$datosImagen', '$imageType')";
            if(executeQueryNoCommit($pdo, $sql)) {
                //Consultar el id autogenerado
                $sql = "SELECT id FROM Foto WHERE imagen = '$datosImagen' && tipo = '$imageType'";
                $rec = getResultSetNoCommit($pdo, $sql);
                $row = mysqli_fetch_object($rec);
                $result = $row;
                
                $set[] = "foto = $result->id";
            }
        }
    } else {
        $interno = getInterno($_POST['nui'], $_POST['td']);
        $foto = $interno->getFoto();
        if($foto->getId() != null) {
            $set[] = "foto = ".$foto->getId();
        } else {
            $set[] = "foto = null";
        }
    }
    
    $updateSet = "";
    for($i=0; $i<sizeof($set); $i++) {
        $updateSet .= $set[$i];
        if($i < sizeof($set)-1) {
            $updateSet .= ", ";
        }
    }
    
    $update = "UPDATE Interno SET $updateSet WHERE nui = '$nui' AND td = '$td'";
    if(executeQueryCommit($pdo, $update)) {
        mostrarMensaje("Interno modificado correctamente");
        if($estado == "A") {
            redirigir("consultarInterno.php?nui=".$nui."&td=".$td);
        } else {
            redirigir("consultarInternos.php");
        }
    }
}