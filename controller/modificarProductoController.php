<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';

if(isset($_POST['register'])) {
    $codigoBarras = $_POST['codigoBarras'];
    $nombre = $_POST['nombre'];
    $grupo = $_POST['selectGrupo'];
    $iva = $_POST['iva'];
    $puntoPedido = $_POST['puntoPedido'];
    $stockMinimo = $_POST['stockMinimo'];
    
    $set = array("puntoPedido = $puntoPedido", "stockMinimo = $stockMinimo");
    
    if($grupo != null) {
        $set[] = "grupo = '$grupo'";
    } else {
        $set[] = "grupo = null";
    }
    if($iva != null) {
        $set[] = "iva = $iva";
    } else {
        $set[] = "iva = null";
    }
    
    $updateSet = "";
    for($i=0; $i<sizeof($set); $i++) {
        $updateSet .= $set[$i];
        if($i < sizeof($set)-1) {
            $updateSet .= ", ";
        }
    }
    
    $update = "UPDATE Producto SET $updateSet WHERE codigoBarras = '$codigoBarras'";
    if(executeSimpleQuery($update)) {
        mostrarMensaje("Producto modificado correctamente");
        redirigir("consultarProducto.php?codigo=".$codigoBarras);
    }
}