<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/DocumentoFuente.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE DocumentoFuente SET estado = 'I' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarDocumentosFuente.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE DocumentoFuente SET estado = 'A' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarDocumentosFuente.php");
    }
} else if(isset($_GET['details'])) {
    redirigir("consultarDocumentoFuente.php?sigla=".$_GET['sigla']);
}

/**
 * Obtiene todos los documentos fuente registrados.
 * @return type
 */
function getDocumentosFuente() {
    $result = traerDocumentosFuente();
    $docsFuente = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $docFuente = almacenarDocumentoFuente($fila);
            $docsFuente[] = $docFuente;
        }
    }
    return $docsFuente;
}

/**
 * Obtiene todos los documentos fuenre activos.
 * @return type
 */
function getDocumentosFuenteActivos($filtro = "") {
    $filtro = "WHERE DF.estado = 'A' ".$filtro;
    $result = traerDocumentosFuente($filtro);
    $docsFuente = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $docFuente = almacenarDocumentoFuente($fila);
            $docsFuente[] = $docFuente;
        }
    }
    return $docsFuente;
}

/**
 * Obtiene la informacion del documento fuente, dada su sigla.
 * @param type $sigla
 * @return type
 */
function getDocumentoFuente($sigla) {
    $filtro = "WHERE DF.sigla = '$sigla'";
    $result = traerDocumentosFuente($filtro);
    $docFuente = null;
    if($result->num_rows > 0) {
        $fila = mysqli_fetch_array($result);
        $docFuente = almacenarDocumentoFuente($fila);
    }
    return $docFuente;
}

/**
 * Almacena el documento fuente traido de la BD.
 * @param type $fila Documento fuente de la BD.
 * @return \DocumentoFuente Objeto tipo DocumentoFuente.
 */
function almacenarDocumentoFuente($fila) {
    $docFuente = new DocumentoFuente($fila['sigla'], $fila['nombre'], $fila['accion'], $fila['aplicaA'], $fila['iva'], $fila['vencimiento'], $fila['estado']);
    return $docFuente;
}

/**
 * Trae los documentos fuente de la BD.
 * @param type $filtro Filtro para buscar un documento fuente específico. Por defecto busca todos los documentos fuente.
 * @return type
 */
function traerDocumentosFuente($filtro = "") {
    $sql = "SELECT DISTINCT * FROM DocumentoFuente DF ";
    return getResultSet($sql.$filtro);
}