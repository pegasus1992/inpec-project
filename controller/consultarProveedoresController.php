<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Proveedor.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE Proveedor SET estado = 'I' WHERE id = " . $_GET['id'];
    if(executeSimpleQuery($update)) {
        redirigir("consultarProveedores.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE Proveedor SET estado = 'A' WHERE id = " . $_GET['id'];
    if(executeSimpleQuery($update)) {
        redirigir("consultarProveedores.php");
    }
}

/**
 * Obtiene todos los proveedores registrados.
 * @return \Proveedor
 */
function getProveedores() {
    $result = traerProveedores();
    $proveedores = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $proveedor = almacenarProveedor($fila);
            $proveedores[] = $proveedor;
        }
    }
    return $proveedores;
}

/**
 * Obtiene todos los proveedores activos.
 * @return type
 */
function getProveedoresActivos() {
    $filtro = "WHERE P.estado = 'A'";
    $result = traerProveedores($filtro);
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $proveedor = almacenarProveedor($fila);
            $proveedores[] = $proveedor;
        }
    }
    return $proveedores;
}

/**
 * Obtiene la informacion del proveedor, dado su id.
 * @param type $id
 * @return type
 */
function getProveedor($id) {
    $filtro = "WHERE P.id = $id";
    $result = traerProveedores($filtro);
    $proveedor = null;
    if($result->num_rows > 0) {
        $fila = mysqli_fetch_array($result);
        $proveedor = almacenarProveedor($fila);
    }
    return $proveedor;
}

/**
 * Almacena el proveedor traido de la BD.
 * @param type $fila Proveedor de la BD.
 * @return \Proveedor Objeto tipo Proveedor.
 */
function almacenarProveedor($fila) {
    $proveedor = new Proveedor($fila['id'], $fila['nit'], $fila['digito'], $fila['nombre'], $fila['email'], $fila['direccion'], $fila['estado']);
    return $proveedor;
}

/**
 * Trae los proveedores de la BD.
 * @param type $filtro Filtro para buscar un proveedor específico. Por defecto busca todos los proveedores.
 * @return type
 */
function traerProveedores($filtro = "") {
    $sql = "SELECT DISTINCT * from Proveedor P ";
    return getResultSet($sql.$filtro);
}