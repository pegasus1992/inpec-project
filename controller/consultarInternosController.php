<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Interno.php';

if(isset($_GET['details'])) {
    redirigir("consultarInterno.php?nui=". $_GET['nui'] ."&td=". $_GET['td']);
}

/**
 * Obtiene todos los internos registrados activos.
 * @return type
 */
function getInternos() {
    $result = traerInternos();
    $internos = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $interno = almacenarInterno($fila);
            $internos[] = $interno;
        }
    }
    return $internos;
}

/**
 * Obtiene la informacion del interno, dado su nui y su td.
 * @param type $nui
 * @param type $td
 * @return \Interno
 */
function getInterno($nui, $td) {
    $filtro = "WHERE I.nui = '$nui' AND I.td = '$td'";
    $result = traerInternos($filtro);
    $interno = null;
    if($result->num_rows > 0) {
        $fila = mysqli_fetch_array($result);
        $interno = almacenarInterno($fila);
    }
    return $interno;
}

/**
 * Almacena el interno traido de la BD.
 * @param type $fila Interno de la BD.
 * @return \Interno Objeto tipo Interno.
 */
function almacenarInterno($fila) {
    $establecimiento = new Establecimiento($fila['tdEstabl'], $fila['nombreEstabl']);
    $foto = new Foto($fila['idImagen'], $fila['imagen'], $fila['tipoImagen']);
    $interno = new Interno($fila['nui'], $fila['td'], $establecimiento, $fila['nombres'], $fila['apellidos'], $fila['nacionalidad'], 
            $fila['situacionJuridica'], $fila['fechaIngreso'], $fila['fechaSalida'], $fila['delito'], $fila['observaciones'], $foto, $fila['estado']);
    return $interno;
}

/**
 * Trae los internos de la BD.
 * @param type $filtro Filtro para buscar un interno específico. Por defecto busca todos los internos.
 * @return type
 */
function traerInternos($filtro = "") {
    $sql = "SELECT E.td tdEstabl, E.nombre nombreEstabl, F.id idImagen, F.imagen, F.tipo tipoImagen, "
            . "I.nui, I.td, I.nombres, I.apellidos, I.nacionalidad, I.situacionJuridica, I.delito, I.observaciones, "
            . "DATE_FORMAT(I.fechaIngreso,'%Y-%m-%d') fechaIngreso, DATE_FORMAT(I.fechaSalida,'%Y-%m-%d') fechaSalida, I.estado "
            . "FROM Interno I LEFT JOIN Foto F ON I.foto = F.id "
            . "LEFT JOIN Establecimiento E ON I.tdEstablecimiento = E.td ";
    return getResultSet($sql.$filtro);
}