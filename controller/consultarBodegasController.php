<?php
include_once 'utils/funciones.php';
initSession();
include_once 'persistance/database.php';
include_once 'model/Bodega.php';

if(isset($_GET['desactivar'])) {
    $update = "UPDATE Bodega SET estado = 'I' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarBodegas.php");
    }
} else if(isset($_GET['reactivar'])) {
    $update = "UPDATE Bodega SET estado = 'A' WHERE sigla = '" . $_GET['sigla'] . "'";
    if(executeSimpleQuery($update)) {
        redirigir("consultarBodegas.php");
    }
}

/**
 * Obtiene todas las bodegas registradas.
 * @return \Bodega
 */
function getBodegas() {
    $sql = "SELECT * FROM Bodega";
    $result = getResultSet($sql);
    $bodegas = array();
    if($result->num_rows > 0) {
        while($fila = mysqli_fetch_array($result)) {
            $bodega = new Bodega($fila['sigla'], $fila['nombre'], $fila['estado']);
            $bodegas[] = $bodega;
        }
    }
    return $bodegas;
}