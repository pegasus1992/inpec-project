<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarProveedoresController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Proveedores</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:840px" class="texto2">
                <h4 align="center">Consultar Proveedores</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>NIT</th>
                        <th>Nombre</th>
                        <th>Correo</th>
                        <th>Dirección</th>
                    </tr>
                    <?php
                    $proveedores = getProveedores();
                    foreach($proveedores as $proveedor) {
                        echo '<tr><form action="" method="get">'
                            . '<td align="center"><input type="text" name="nit" style="width:100px" value="' . $proveedor->getNit()."-".$proveedor->getDigito() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $proveedor->getNombre() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="correo" style="width:200px" value="' . $proveedor->getCorreo() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="direccion" style="width:200px" value="' . $proveedor->getDireccion() . '" readonly></td>'
                            . '<td align="center" style="border:0px"><input type="text" name="id" style="width:0px; border:0px" value="' . $proveedor->getId() . '" readonly></td>';
                        if($proveedor->getEstado() == "A") {
                            echo '<td align="center"><input name="desactivar" type="submit" value="Desactivar" class="Button"></td>';
                        } else {
                            echo '<td align="center"><input name="reactivar" type="submit" value="Reactivar" class="Button"></td>';
                        }
                        echo '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}