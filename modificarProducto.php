<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarProductosController.php';
include_once 'controller/modificarProductoController.php';
include_once 'controller/consultarGruposController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Modificar Producto</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $producto = getProducto($_POST['codigo']);
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Modificar Producto</h4>
                <div>
                    <label for="codigoBarras">Código de Barras:</label>
                    <?php echo '<input name="codigoBarras" id="codigoBarras" type="text" value="'.$producto->getCodigoBarras().'" readonly/>' ?>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <?php echo '<input name="nombre" id="nombre" type="text" value="'.$producto->getNombre().'" readonly/>' ?>
                </div>
                <div>
                    <label>Grupo de Productos:</label>
                    <select name="selectGrupo" id="selectGrupo">
                        <option></option>
                        <?php
                        $filtro = "WHERE estado = 'A'";
                        $grupos = getGrupos($filtro);
                        foreach($grupos as $grupo) {
                            $siglaGrupo = $grupo->getSigla();
                            $nombreGrupo = $grupo->getNombre();
                            if($siglaGrupo == $producto->getGrupo()->getSigla()) {
                                echo '<option value="'.$siglaGrupo.'" selected>'.$nombreGrupo.'</option>';
                            } else {
                                echo '<option value="'.$siglaGrupo.'">'.$nombreGrupo.'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
                <!--p>
                    <label for="iva">IVA:</label>
                    <!--?php echo '<input name="iva" id="iva" type="number" value="'.$producto->getIva().'" autocomplete="off"/>' ?>
                </p-->
                <div>
                    <label for="puntoPedido">Punto de Pedido (*):</label>
                    <?php echo '<input name="puntoPedido" id="puntoPedido" type="number" value="'.$producto->getPuntoPedido().'" required min="0" autocomplete="off"/>' ?>
                </div>
                <div>
                    <label for="stockMinimo">Stock Mínimo (*):</label>
                    <?php echo '<input name="stockMinimo" id="stockMinimo" type="number" value="'.$producto->getStockMinimo().'" required min="0" autocomplete="off"/>' ?>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Modificar" class="Button2"/>
                    <?php
                    echo '<input name="return" type="button" value="Regresar" class="Button2" onclick="window.location=\'consultarProducto.php?codigo='.$producto->getCodigoBarras().'\'">';
                    ?>
                </div>
                
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}