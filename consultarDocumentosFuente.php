<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarDocumentosFuenteController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Documentos Fuente</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:650px" class="texto2">
                <h4 align="center">Consultar Documentos Fuente</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>Sigla</th>
                        <th>Nombre</th>
                        <th>Actúa en</th>
                    </tr>
                    <?php
                    $docsFuente = getDocumentosFuente();
                    foreach($docsFuente as $docFuente) {
                        echo '<tr><form action="" method="get">'
                            . '<td align="center"><input type="text" name="sigla" style="width:100px" value="' . $docFuente->getSigla() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $docFuente->getNombre() . '" readonly></td>';
                        if($docFuente->getAccion() == "E") {
                            echo '<td align="center"><input type="text" name="accion" style="width:100px" value="Entradas" readonly></td>';
                        } else {
                            echo '<td align="center"><input type="text" name="accion" style="width:100px" value="Salidas" readonly></td>';
                        }
                        if($docFuente->getEstado() == "A") {
                            echo '<td align="center"><input name="details" type="submit" value="Más Detalles" class="Button"></td>'
                            . '<td align="center"><input name="desactivar" type="submit" value="Desactivar" class="Button"></td>';
                        } else {
                            echo '<td align="center"></td>'
                            . '<td align="center"><input name="reactivar" type="submit" value="Reactivar" class="Button"></td>';
                        }
                        echo '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}