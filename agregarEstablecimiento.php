<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarEstablecimientoController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar Establecimiento</title>';
        ?>
    </head>
    <body>
        <h1>Agregar Establecimiento</h1>
        <form action="" method="post" enctype="multipart/form-data">
            <p>
                <label for="nombre">Nombre del establecimiento:</label>
                <input placeholder="Ingrese el nombre" name="nombre" id="nombre" type="text" autocomplete="off" required/>
            </p>
            <input name="register" id="submit" type="submit" value="Registrar"/>
            <input name="return" type="button" value="Regresar" onclick="window.location='./'">
        </form>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}