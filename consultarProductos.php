<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarProductosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Productos</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:700px" class="texto2">
                <h4 align="center">Consultar Productos</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>Código de Barras</th>
                        <th>Nombre del Producto</th>
                        <th>Grupo del Producto</th>
                    </tr>
                    <?php
                    $productos = getProductos();
                    foreach($productos as $producto) {
                        echo '<tr><form action="" method="get">'
                            . '<td align="center"><input type="text" name="codigo" style="width:150px" value="' . $producto->getCodigoBarras() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $producto->getNombre() . '" readonly></td>'
                            . '<td align="center"><input type="text" name="grupo" style="width:100px" value="' . $producto->getGrupo()->getNombre() . '" readonly></td>';
                        if($producto->getEstado() == "A") {
                            echo '<td align="center"><input name="details" type="submit" value="Más Detalles" class="Button"></td>'
                            . '<td align="center"><input name="desactivar" type="submit" value="Desactivar" class="Button"></td>';
                        } else {
                            echo '<td align="center"></td>'
                            . '<td align="center"><input name="reactivar" type="submit" value="Reactivar" class="Button"></td>';
                        }
                        echo '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}