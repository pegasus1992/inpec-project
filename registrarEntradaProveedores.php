<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarDocumentosFuenteController.php';
include_once 'controller/consultarProveedoresController.php';
include_once 'controller/consultarProductosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Registrar Entradas de Proveedor</title>';
        ?>
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
    </head>
    <body>
        <h1>Registrar Entradas de Proveedor</h1>
        <form action="" method="post" enctype="multipart/form-data">
            <p>
                <label>Documento de salida:</label>
                <select name="selectDoc" id="selectDoc" required>
                    <option></option>
                    <?php
                    $filtro = "AND DF.accion = 'E'";
                    $docsFuente = getDocumentosFuenteActivos($filtro);
                    foreach($docsFuente as $docFuente) {
                        echo '<option value="'.$docFuente->getSigla().'">'.$docFuente->getNombre().'</option>';
                    }
                    ?>
                </select>
            </p>
            <p>
                <label>Proveedor:</label>
                <select name="selectProveedor" id="selectProveedor" required>
                    <option></option>
                    <?php
                    $proveedores = getProveedoresActivos();
                    foreach($proveedores as $proveedor) {
                        $proveedorJson = replaceJson2Html($proveedor->parseJson());
                        echo '<option value="'.$proveedorJson.'">'.$proveedor->getNombre().'</option>';
                    }
                    ?>
                </select>
            </p>
            <p>
                <label for="nitProveedor">NIT del Proveedor:</label>
                <input name="nitProveedor" id="nitProveedor" type="text" value="" readonly/>
            </p>
            <p>
                <label for="nombreProveedor">Nombre del Proveedor:</label>
                <input name="nombreProveedor" id="nombreProveedor" type="text" value="" readonly/>
            </p>
            <p>
                <label for="correoProveedor">Correo del Proveedor:</label>
                <input name="correoProveedor" id="correoProveedor" type="text" value="" readonly/>
            </p>
            <p>
                <label for="direccionProveedor">Dirección del Proveedor:</label>
                <input name="direccionProveedor" id="direccionProveedor" type="text" value="" readonly/>
            </p>
            <table id="tabla" border="1">
                <tr>
                    <th>Código de barras</th>
                    <th>Descripción</th>
                    <th>Bodega</th>
                    <th>Cantidad</th>
                    <th>Valor unitario</th>
                    <th>Valor total</th>
                    <th>IVA (%)</th>
                </tr>
                <tr>
                    <td align="center">
                        <select name="codigoBarras" class="codigoBarras" required>
                            <option></option>
                            <?php
                            $productos = getProductosActivos();
                            foreach($productos as $producto) {
                                $productoJson = replaceJson2Html($producto->parseJson());
                                echo '<option value="'.$productoJson.'">'.$producto->getCodigoBarras().'</option>';
                            }
                            ?>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="nombreProducto" class="nombreProducto" style="width:125px" value="" readonly>
                    </td>
                    <td>
                        <!-- BODEGA -->
                    </td>
                    <td>
                        <!-- CANTIDAD -->
                    </td>
                    <td>
                        <!-- VALOR UNITARIO -->
                    </td>
                    <td>
                        <!-- VALOR TOTAL -->
                    </td>
                    <td>
                        <input type="number" name="ivaProducto" class="ivaProducto" style="width:60px" value="" readonly>
                    </td>
                </tr>
            </table>
            <input name="register" id="submit" type="submit" value="Registrar"/>
            <input name="return" type="button" value="Regresar" onclick="window.location='./'">
        </form>
        <!-- SCRIPTS VARIOS -->
        <script src="view/js/registrarMovimientoProveedor.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}