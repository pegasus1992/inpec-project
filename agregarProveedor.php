<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarProveedorController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar Proveedor</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Agregar Proveedor</h4>
                <div>
                    <label for="nui">NIT:</label>
                    <input placeholder="#" name="digito" id="digito" type="number" autocomplete="off" min="1" max="9" required/>
                    <input placeholder="Ingrese el NIT" name="nit" id="nit" type="number" autocomplete="off" min="1" max="9999999999" required/>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <input placeholder="Ingrese el nombre" name="nombre" id="nombre" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="correo">Correo:</label>
                    <input placeholder="Ingrese el correo" type="email" name="correo" id="correo" autocomplete="off" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" required/>
                </div>
                <div>
                    <label for="direccion">Dirección:</label>
                    <input placeholder="Ingrese la dirección" name="direccion" id="direccion" type="text" autocomplete="off"/>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}