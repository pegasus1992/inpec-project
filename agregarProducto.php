<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/agregarProductoController.php';
include_once 'controller/consultarGruposController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Agregar Producto</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Agregar Producto</h4>
                <div>
                    <label for="codigo">Código de Barras (*):</label>
                    <input placeholder="Ingrese el código de barras" name="codigo" id="codigo" type="text" autocomplete="off" maxlength="15" required/>
                </div>
                <div>
                    <label for="nombre">Nombre (*):</label>
                    <input placeholder="Ingrese el nombre" name="nombre" id="nombre" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label>Grupo de Productos:</label>
                    <select name="selectGrupo" id="selectGrupo">
                        <option></option>
                        <?php
                        $filtro = "WHERE estado = 'A'";
                        $grupos = getGrupos($filtro);
                        foreach($grupos as $grupo) {
                            $siglaGrupo = $grupo->getSigla();
                            $nombreGrupo = $grupo->getNombre();
                            echo '<option value="'.$siglaGrupo.'">'.$nombreGrupo.'</option>';
                        }
                        ?>
                    </select>
                </div>
                <!--p>
                    <label for="iva">IVA:</label>
                    <input placeholder="Ingrese el IVA (solo números, sin %)" name="iva" id="iva" type="number" autocomplete="off"/>
                </p-->
                <div>
                    <label for="puntoPedido">Punto de Pedido (*):</label>
                    <input placeholder="Ingrese el punto de pedido" name="puntoPedido" id="puntoPedido" type="number" min="0" autocomplete="off" required/>
                </div>
                <div>
                    <label for="stockMinimo">Stock Mínimo (*):</label>
                    <input placeholder="Ingrese el stock minimo" name="stockMinimo" id="stockMinimo" type="number" min="0" autocomplete="off" required/>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}