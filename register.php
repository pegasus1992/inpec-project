<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/registerController.php';

if(!isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Página de Registro</title>';
        ?>
        <script src="view/js/encryption.js" type="text/javascript"></script>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <form action="" method="post" class="registro">
                <h4 align="center">Página de Registro</h4>
                <div>
                    <label>Tipo de Documento:</label>
                    <select name="selectTipoDoc" id="selectTipoDoc" required>
                        <option></option>
                        <option value="CC">Cédula de Ciudadanía</option>
                        <option value="CE">Cédula de Extranjería</option>
                    </select>
                </div>
                <div>
                    <label for="numeroDocumento">Numero de Documento:</label>
                    <input placeholder="Ingrese su número de documento" name="numDoc" id="numDoc" type="text" autocomplete="off" maxlength="15" required/>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <input placeholder="Ingrese su nombre" name="name" id="name" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="correo">Correo:</label>
                    <input placeholder="Ingrese su correo" type="email" name="correo" id="correo" autocomplete="off" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" required/>
                </div>
                <div>
                    <label for="username">Nombre de Usuario:</label>
                    <input placeholder="Ingrese su nombre de usuario" name="username" id="username" type="text" autocomplete="off" required/>
                </div>
                <div>
                    <label for="password">Contraseña:</label>
                    <input placeholder="Ingrese su contraseña" name="password" id="password" type="password" value="" autocomplete="off" required/>
                </div>
                <div>
                    <label for="repassword">Repetir contraseña:</label>
                    <input placeholder="Repita su contraseña" name="repassword" id="repassword" type="password" value="" autocomplete="off" required/>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Registrar" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
            </form>
        </div>
        <!-- REGISTER SCRIPTS -->
        <script src="view/js/registerScripts.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir('home.php');
}