<?php

class Establecimiento {
    private $td, $nombre;
    
    public function Establecimiento($td, $nombre) {
        $this->setTd($td);
        $this->setNombre($nombre);
    }
    
    public function getTd() {
        return $this->td;
    }

    public function getNombre() {
        return $this->nombre;
    }

    public function setTd($td) {
        $this->td = $td;
    }

    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
}