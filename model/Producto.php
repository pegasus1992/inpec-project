<?php
include_once 'Grupo.php';

class Producto {
    private $codigoBarras, $nombre, $grupo, $iva, $puntoPedido, $stockMinimo, $estado;
    
    public Function Producto($codigoBarras, $nombre, $grupo, $iva, $puntoPedido, $stockMinimo, $estado) {
        $this->setCodigoBarras($codigoBarras);
        $this->setNombre($nombre);
        $this->setGrupo($grupo);
        $this->setIva($iva);
        $this->setPuntoPedido($puntoPedido);
        $this->setStockMinimo($stockMinimo);
        $this->setEstado($estado);
    }
    
    function getEstado() {
        return $this->estado;
    }
    
    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    function getCodigoBarras() {
        return $this->codigoBarras;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getGrupo() {
        return $this->grupo;
    }

    function getIva() {
        return $this->iva;
    }

    function getPuntoPedido() {
        return $this->puntoPedido;
    }

    function getStockMinimo() {
        return $this->stockMinimo;
    }

    function setCodigoBarras($codigoBarras) {
        $this->codigoBarras = $codigoBarras;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setGrupo($grupo) {
        $this->grupo = $grupo;
    }

    function setIva($iva) {
        $this->iva = $iva;
    }

    function setPuntoPedido($puntoPedido) {
        $this->puntoPedido = $puntoPedido;
    }

    function setStockMinimo($stockMinimo) {
        $this->stockMinimo = $stockMinimo;
    }
    
    public function parseJson() {
        $producto = array(
            'codigoBarras' => $this->getCodigoBarras(),
            'nombre' => $this->getNombre(),
            'grupo' => $this->getGrupo()->parseJson(),
            'iva' => $this->getIva(),
            'puntoPedido' => $this->getPuntoPedido(),
            'stockMinimo' => $this->getStockMinimo(),
            'estado' => $this->getEstado()
        );
        return json_encode($producto);
    }
}