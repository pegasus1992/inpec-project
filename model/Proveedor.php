<?php

class Proveedor {
    private $id, $nit, $digito, $nombre, $correo, $direccion, $estado;
    
    public function Proveedor($id, $nit, $digito, $nombre, $correo, $direccion, $estado) {
        $this->setId($id);
        $this->setNit($nit);
        $this->setDigito($digito);
        $this->setNombre($nombre);
        $this->setCorreo($correo);
        $this->setDireccion($direccion);
        $this->setEstado($estado);
    }
    
    function getId() {
        return $this->id;
    }

    function getNit() {
        return $this->nit;
    }

    function getDigito() {
        return $this->digito;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getCorreo() {
        return $this->correo;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getEstado() {
        return $this->estado;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNit($nit) {
        $this->nit = $nit;
    }

    function setDigito($digito) {
        $this->digito = $digito;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setCorreo($correo) {
        $this->correo = $correo;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    public function parseJson() {
        $proveedor = array(
            'id' => $this->getId(),
            'nit' => $this->getNit(),
            'digito' => $this->getDigito(),
            'nombre' => $this->getNombre(),
            'email' => $this->getCorreo(),
            'direccion' => $this->getDireccion(),
            'estado' => $this->getEstado()
        );
        return json_encode($proveedor);
    }
}