<?php
include_once 'Foto.php';
include_once 'Establecimiento.php';

class Interno {
    private $nui, $td, $establecimiento, $nombres, $apellidos, $nacionalidad, $situacionJuridica, $fechaIngreso, $fechaSalida, $delito, $observaciones, $foto, $estado;
    
    public function Interno($nui, $td, $establecimiento, $nombres, $apellidos, $nacionalidad, $situacionJuridica, $fechaIngreso, $fechaSalida, $delito, $observaciones, $foto, $estado) {
        $this->setNui($nui);
        $this->setTd($td);
        $this->setEstablecimiento($establecimiento);
        $this->setNombres($nombres);
        $this->setApellidos($apellidos);
        $this->setNacionalidad($nacionalidad);
        $this->setSituacionJuridica($situacionJuridica);
        $this->setFechaIngreso($fechaIngreso);
        $this->setFechaSalida($fechaSalida);
        $this->setDelito($delito);
        $this->setObservaciones($observaciones);
        $this->setFoto($foto);
        $this->setEstado($estado);
    }
 
    public function getEstado() {
        return $this->estado;
    }

    public function setEstado($estado) {
        $this->estado = $estado;
    }
     
    public function getNacionalidad() {
        return $this->nacionalidad;
    }
    
    public function setNacionalidad($nacionalidad) {
        $this->nacionalidad = $nacionalidad;
    }
     
    public function getNui() {
        return $this->nui;
    }

    public function getTd() {
        return $this->td;
    }

    public function getEstablecimiento() {
        return $this->establecimiento;
    }

    public function getNombres() {
        return $this->nombres;
    }

    public function getApellidos() {
        return $this->apellidos;
    }

    public function getSituacionJuridica() {
        return $this->situacionJuridica;
    }

    public function getFechaIngreso() {
        return $this->fechaIngreso;
    }

    public function getFechaSalida() {
        return $this->fechaSalida;
    }

    public function getDelito() {
        return $this->delito;
    }

    public function getObservaciones() {
        return $this->observaciones;
    }

    public function getFoto() {
        return $this->foto;
    }

    public function setNui($nui) {
        $this->nui = $nui;
    }

    public function setTd($td) {
        $this->td = $td;
    }

    public function setEstablecimiento($establecimiento) {
        $this->establecimiento = $establecimiento;
    }

    public function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    public function setApellidos($apellidos) {
        $this->apellidos = $apellidos;
    }

    public function setSituacionJuridica($situacionJuridica) {
        $this->situacionJuridica = $situacionJuridica;
    }

    public function setFechaIngreso($fechaIngreso) {
        $this->fechaIngreso = $fechaIngreso;
    }

    public function setFechaSalida($fechaSalida) {
        $this->fechaSalida = $fechaSalida;
    }

    public function setDelito($delito) {
        $this->delito = $delito;
    }

    public function setObservaciones($observaciones) {
        $this->observaciones = $observaciones;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }
}