<?php

class Bodega {
    private $sigla, $nombre, $estado;
    
    public function Bodega($sigla, $nombre, $estado) {
        $this->setSigla($sigla);
        $this->setNombre($nombre);
        $this->setEstado($estado);
    }
    
    function getEstado() {
        return $this->estado;
    }
    
    function setEstado($estado) {
        $this->estado = $estado;
    }
    
    function getSigla() {
        return $this->sigla;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }
}