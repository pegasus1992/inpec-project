<?php

class DocumentoFuente {
    private $sigla, $nombre, $accion, $aplica, $iva, $vencimiento, $estado;
    
    public function DocumentoFuente($sigla, $nombre, $accion, $aplica, $iva, $vencimiento, $estado) {
        $this->setSigla($sigla);
        $this->setNombre($nombre);
        $this->setAccion($accion);
        $this->setAplica($aplica);
        $this->setIva($iva);
        $this->setVencimiento($vencimiento);
        $this->setEstado($estado);
    }
    
    function getSigla() {
        return $this->sigla;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getAccion() {
        return $this->accion;
    }

    function getAplica() {
        return $this->aplica;
    }

    function getIva() {
        return $this->iva;
    }

    function getVencimiento() {
        return $this->vencimiento;
    }

    function getEstado() {
        return $this->estado;
    }

    function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setAccion($accion) {
        $this->accion = $accion;
    }

    function setAplica($aplica) {
        $this->aplica = $aplica;
    }

    function setIva($iva) {
        $this->iva = $iva;
    }

    function setVencimiento($vencimiento) {
        $this->vencimiento = $vencimiento;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }
}