<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'imagen.php';
include_once 'controller/consultarInternosController.php';
include_once 'controller/modificarInternoController.php';
include_once 'controller/consultarEstablecimientosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Modificar Interno</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $interno = getInterno($_POST['nui'], $_POST['td']);
            ?>
            <form action="" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Modificar Interno</h4>
                <div>
                    <label for="nui">Numero NUI:</label>
                    <?php echo '<input name="nui" id="nui" type="number" value="'.$interno->getNui().'" readonly/>' ?>
                </div>
                <div>
                    <label for="td">Numero TD:</label>
                    <?php echo '<input name="td" id="td" type="text" value="'.$interno->getTd().'" readonly/>' ?>
                </div>
                <!--p>
                    <label>Establecimiento:</label>
                    <select name="selectEstablecimiento" id="selectEstablecimiento" required>
                        <option></option>
                        <!--?php
                        $establecimientos = getEstablecimientos();
                        foreach($establecimientos as $establecimiento) {
                            $td = $establecimiento->getTd();
                            $nombre = $establecimiento->getNombre();
                            if($td == $interno->getEstablecimiento()->getTd()) {
                                echo '<option value="'.$td.'" selected>'.$nombre.'</option>';
                            } else {
                                echo '<option value="'.$td.'">'.$nombre.'</option>';
                            }
                        }
                        ?>
                    </select>
                </p-->
                <div>
                    <label for="nombres">Nombres (*):</label>
                    <?php echo '<input name="nombres" id="nombres" type="text" value="'.$interno->getNombres().'" required autocomplete="off"/>' ?>
                </div>
                <div>
                    <label for="apellidos">Apellidos (*):</label>
                    <?php echo '<input name="apellidos" id="apellidos" type="text" value="'.$interno->getApellidos().'" required autocomplete="off"/>' ?>
                </div>
                <div>
                    <label for="nacionalidad">Nacionalidad:</label>
                    <?php echo '<input name="nacionalidad" id="nacionalidad" type="text" value="'.$interno->getNacionalidad().'" autocomplete="off"/>' ?>
                </div>
                <div>
                    <label for="situacion">Situación Jurídica:</label>
                    <select name="situacion" id="situacion">
                        <option></option>
                        <?php
                        if($interno->getSituacionJuridica() == "S") {
                            echo '<option value="S" selected>Sindicado</option>';
                            echo '<option value="C">Condenado</option>';
                        } else if($interno->getSituacionJuridica() == "C") {
                            echo '<option value="S">Sindicado</option>';
                            echo '<option value="C" selected>Condenado</option>';
                        } else {
                            echo '<option value="S">Sindicado</option>';
                            echo '<option value="C">Condenado</option>';
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <label for="delito">Delito (*):</label>
                    <?php echo '<textarea cols="40" rows="5" name="delito" id="delito" type="text" required autocomplete="off">'.$interno->getDelito().'</textarea>' ?>
                </div>
                <div>
                    <label for="observaciones">Observaciones:</label>
                    <?php echo '<textarea cols="40" rows="5" name="observaciones" id="observaciones" type="text" autocomplete="off">'.$interno->getObservaciones().'</textarea>' ?>
                </div>
                <div>
                    <label for="fechaIngreso">Fecha de Ingreso:</label>
                    <?php echo '<input name="fechaIngreso" id="fechaIngreso" type="date" value="'.$interno->getFechaIngreso().'" autocomplete="off" placeholder="yyyy-mm-dd"/>' ?>
                </div>
                <div>
                    <label for="fechaSalida">Fecha de Salida:</label>
                    <?php echo '<input name="fechaSalida" id="fechaSalida" type="date" value="'.$interno->getFechaSalida().'" autocomplete="off" placeholder="yyyy-mm-dd"/>' ?>
                </div>
                <div>
                    <label>Estado:</label>
                    <select name="selectEstado" id="selectEstado" required>
                        <?php
                        if($interno->getEstado() == "A") {
                            echo '<option value="A" selected>Activo</option>';
                            echo '<option value="I">Inactivo</option>';
                        } else {
                            echo '<option value="A">Activo</option>';
                            echo '<option value="I" selected>Inactivo</option>';
                        }
                        ?>
                    </select>
                </div>
                <div>
                    <label for="foto">Foto:</label>
                    <input placeholder="Ingrese la foto" name="foto" id="foto" type="file" accept="image/*" autocomplete="off"/>
                    <br/>
                    <br/>
                    <?php
                    $foto = $interno->getFoto();
                    if($foto->getId() != null) {
                        echo imageField("imagen", $foto->getId());
                    } else {
                        echo imageField("imagen");
                    }
                    ?>
                </div>
                <div align="center">
                    <input name="register" id="submit" type="submit" value="Modificar" class="Button2"/>
                    <?php
                    echo '<input name="return" type="button" value="Regresar" class="Button2" onclick="window.location=\'consultarInterno.php?nui='.$interno->getNui().'&td='.$interno->getTd().'\'">';
                    ?>
                </div>
            </form>
        </div>
        <!-- CARGAR IMAGEN -->
        <script src="view/js/fotoInternoCharger.js" type="text/javascript"></script>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}