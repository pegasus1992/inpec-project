<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarDocumentosFuenteController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Documento Fuente</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $docFuente = getDocumentoFuente($_GET['sigla']);
            ?>
            <form action="modificarDocumentoFuente.php" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Consultar Documento Fuente</h4>
                <div>
                    <label for="sigla">Sigla:</label>
                    <?php echo '<input name="sigla" id="sigla" type="text" value="'.$docFuente->getSigla().'" readonly/>' ?>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <?php echo '<input name="nombre" id="nombre" type="text" value="'.$docFuente->getNombre().'" readonly/>' ?>
                </div>
                <div>
                    <label>Actúa en:</label>
                    <?php
                    $accion = $docFuente->getAccion();
                    if($accion == "E") {
                        echo '<input name="accion" id="accion" type="text" value="Entradas" readonly/>';
                    } else {
                        echo '<input name="accion" id="accion" type="text" value="Salidas" readonly/>';
                    }
                    ?>
                </div>
                <div>
                    <label>Aplica a:</label>
                    <?php
                    $aplica = $docFuente->getAplica();
                    if($aplica == "P") {
                        echo '<input name="aplica" id="aplica" type="text" value="Proveedores" readonly/>';
                    } else {
                        echo '<input name="aplica" id="aplica" type="text" value="Clientes" readonly/>';
                    }
                    ?>
                </div>
                <div>
                    <label>¿Tiene IVA? </label>
                    <?php
                    $iva = $docFuente->getIva();
                    if($iva == "S") {
                        echo '<input name="iva" id="iva" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="iva" id="iva" type="text" value="No" readonly/>';
                    }
                    ?>
                </div>
                <div>
                    <label>¿Tiene vencimiento? </label>
                    <?php
                    $vencimiento = $docFuente->getVencimiento();
                    if($vencimiento == "S") {
                        echo '<input name="vencimiento" id="vencimiento" type="text" value="Sí" readonly/>';
                    } else {
                        echo '<input name="vencimiento" id="vencimiento" type="text" value="No" readonly/>';
                    }
                    ?>
                </div>
                <div align="center">
                    <input name="modify" id="submit" type="submit" value="Modificar Documento Fuente" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='consultarDocumentosFuente.php'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}