<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarInternosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Internos</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:900px" class="texto2">
                <h4 align="center">Consultar Internos</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>NUI</th>
                        <th>TD</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Situación Jurídica</th>
                        <th>Estado</th>
                    </tr>
                    <?php
                    $internos = getInternos();
                    foreach($internos as $interno) {
                        echo '<tr><form action="" method="get">'
                                . '<td align="center"><input type="text" name="nui" style="width:100px" value="' . $interno->getNui() . '" readonly></td>'
                                . '<td align="center"><input type="text" name="td" style="width:100px" value="' . $interno->getTd() . '" readonly></td>'
                                . '<td align="center"><input type="text" name="nombres" style="width:200px" value="' . $interno->getNombres() . '" readonly></td>'
                                . '<td align="center"><input type="text" name="apellidos" style="width:200px" value="' . $interno->getApellidos() . '" readonly></td>';
                        if($interno->getSituacionJuridica() == "S") {
                            echo '<td align="center"><input type="text" name="situacion" style="width:100px" value="Sindicado" readonly></td>';
                        } else if($interno->getSituacionJuridica() == "C") {
                            echo '<td align="center"><input type="text" name="situacion" style="width:100px" value="Condenado" readonly></td>';
                        } else {
                            echo '<td align="center"><input type="text" name="situacion" style="width:100px" value="" readonly></td>';
                        }
                        if($interno->getEstado() == "A") {
                            echo '<td align="center"><input type="text" name="apellidos" style="width:50px" value="Activo" readonly></td>';
                        } else {
                            echo '<td align="center"><input type="text" name="apellidos" style="width:50px" value="Inactivo" readonly></td>';
                        }
                        echo '<td align="center"><input name="details" type="submit" value="Más Detalles" class="Button"></td>'
                        . '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}