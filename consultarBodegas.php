<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarBodegasController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Bodegas</title>';
        ?>
        <link href="view/css/main.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <div style="width:425px" class="texto2">
                <h4 align="center">Consultar Bodegas</h4>
                <table border="0" class="asociados">
                    <tr>
                        <th>Sigla</th>
                        <th>Nombre</th>
                    </tr>
                    <?php
                    $bodegas = getBodegas();
                    foreach($bodegas as $bodega) {
                        echo '<tr><form action="" method="get">'
                        . '<td align="center"><input type="text" name="sigla" style="width:100px" value="' . $bodega->getSigla() . '" readonly></td>'
                        . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $bodega->getNombre() . '" readonly></td>';
                    if($bodega->getEstado() == "A") {
                        echo '<td align="center"><input name="desactivar" type="submit" value="Desactivar" class="Button"></td>';
                    } else {
                        echo '<td align="center"><input name="reactivar" type="submit" value="Reactivar" class="Button"></td>';
                    }
                    echo '</form></tr>';
                    }
                    ?>
                </table>
                <br/>
                <div align="center">
                    <input name="return" type="button" value="Regresar" onclick="window.location='./'" class="Button2">
                </div>
                <br/>
            </div>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}