<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarEstablecimientosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Establecimientos</title>';
        ?>
    </head>
    <body>
        <h1>Consultar Establecimientos</h1>
        <table border="1">
            <tr>
                <th>TD</th>
                <th>Nombre</th>
            </tr>
            <?php
            $establecimientos = getEstablecimientos();
            foreach($establecimientos as $establecimiento) {
                echo '<tr>'
                . '<td align="center"><input type="text" name="td" style="width:100px" value="' . $establecimiento->getTd() . '" readonly></td>'
                . '<td align="center"><input type="text" name="nombre" style="width:200px" value="' . $establecimiento->getNombre() . '" readonly></td>'
                . '</tr>';
            }
            ?>
        </table>
        <br/>
        <input name="return" type="button" value="Regresar" onclick="window.location='./'">
    </body>
</html>
    <?php
} else {
    redirigir("./");
}