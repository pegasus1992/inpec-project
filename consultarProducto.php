<?php
include_once 'utils/funciones.php';
initSession();
include_once 'utils/constantes.php';
include_once 'controller/consultarProductosController.php';

if(isset($_SESSION['username'])) {
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <?php
        echo '<title>'.PROJECT_NAME.' - Consultar Producto</title>';
        ?>
        <link href="view/css/registro.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="contenedor">
            <?php
            $producto = getProducto($_GET['codigo']);
            ?>
            <form action="modificarProducto.php" method="post" enctype="multipart/form-data" class="registro">
                <h4 align="center">Consultar Producto</h4>
                <div>
                    <label for="codigo">Código de Barras:</label>
                    <?php echo '<input name="codigo" id="codigo" type="text" value="'.$producto->getCodigoBarras().'" readonly/>' ?>
                </div>
                <div>
                    <label for="nombre">Nombre:</label>
                    <?php echo '<input name="nombre" id="nombre" type="text" value="'.$producto->getNombre().'" readonly/>' ?>
                </div>
                <div>
                    <label for="grupo">Grupo de Productos:</label>
                    <?php echo '<input name="grupo" id="grupo" type="text" value="'.$producto->getGrupo()->getNombre().'" readonly/>' ?>
                </div>
                <!--p>
                    <label for="iva">IVA:</label>
                    <!--?php echo '<input name="iva" id="iva" type="number" value="'.$producto->getIva().'" readonly/>' ?>
                </p-->
                <div>
                    <label for="puntoPedido">Punto de Pedido:</label>
                    <?php echo '<input name="puntoPedido" id="puntoPedido" type="number" value="'.$producto->getPuntoPedido().'" readonly/>' ?>
                </div>
                <div>
                    <label for="stockMinimo">Stock Mínimo:</label>
                    <?php echo '<input name="stockMinimo" id="stockMinimo" type="number" value="'.$producto->getStockMinimo().'" readonly/>' ?>
                </div>
                <div align="center">
                    <input name="modify" id="submit" type="submit" value="Modificar Producto" class="Button2"/>
                    <input name="return" type="button" value="Regresar" onclick="window.location='consultarProductos.php'" class="Button2">
                </div>
            </form>
        </div>
    </body>
</html>
    <?php
} else {
    redirigir("./");
}