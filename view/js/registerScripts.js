document.getElementById("submit").onclick = function () {
    var tipoDoc = document.getElementById("selectTipoDoc").value;
    var numDoc = document.getElementById("numDoc").value;
    var nombre = document.getElementById("name").value;
    var correo = document.getElementById("correo").value;
    var username = document.getElementById("username").value;
    var pass = document.getElementById("password").value;
    var repass = document.getElementById("repassword").value;
    if (isNotNull(tipoDoc) && isNotNull(numDoc) && isNotNull(nombre) && isNotNull(correo)
            && isNotNull(username) && isNotNull(pass) && isNotNull(repass)) {
        document.getElementById("password").value = sha1(pass);
        document.getElementById("repassword").value = sha1(repass);
    }
};